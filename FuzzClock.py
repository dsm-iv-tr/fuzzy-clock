#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
	FuzzClock.py: A fuzzy clock for the GNOME panel.

	This is the main applet, FuzzClock.py.

	Copyright 2008 Troy Rennie <dsm.iv.tr@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import pygtk
pygtk.require('2.0')

import sys
from datetime import datetime
import time

import gobject
import gtk

import gnomeapplet

# Globals: numerics to strings, timeways to strings.
#	TODO: Make the position tuple a user pref.
#
timeways = ( "ten", "quarter", "twenty", "twenty-five", "half" )
position = ( " past ", " to ", " of " )
numerics = { 0: "midnight", 1: "one", 2: "two", 3: "three", 4: "four", 5: "five", 6: "six", 7: "seven", 8: "eight", 9: "nine", 10: "ten", 11: "eleven", 12: "twelve", 13: "thirteen", 14: "fourteen", 15: "fifteen", 16: "sixteen",17: "seventeen", 18: "eighteen", 19: "nineteen", 20: "twenty", 21: "twenty-one", 22: "twenty-two", 23: "twenty-three", 24: "twenty-four" }
mode = 12
buttn_refer = None

def fuzzclock_factory(applet, iid):
	""" This is where the applet gets init'd, and the callback is set to trigger every minute."""
	global buttn_refer

	buttn = gtk.Button("...")

	# Make accessible to a global.
	#
	buttn_refer = buttn

	buttn.set_relief(gtk.RELIEF_NONE)
	buttn.connect("button_press_event", show_clock_menu, applet)

	applet.add(buttn)

	applet.show_all()

	# Display initial time.
	#
	update_fuzzlabel(buttn)

	# Schedule a recheck of time every minute.
	# This seems lazy to me, but threading this just about
	# drove me crazy becaise I don't get how threading works in Python.
	# 	TODO: Make the timeout value a user preference?
	#
	to_retn = gobject.timeout_add(60000, update_fuzzlabel, buttn)

	return gtk.TRUE

def update_fuzzlabel(label_obj):
	""" This function does the actual updating of the label passed to it. """
	label_obj.set_label(update_clock())
	return True

def show_clock_menu(widg, event, applet):
	""" Shows the popup menu to quit, etc. """
	if event.type == gtk.gdk.BUTTON_PRESS and event.button == 3:
		widg.emit_stop_by_name("button_press_event")
		create_the_menu(applet)

def create_the_menu(applet):
	""" Creates a menu to pop up on-the-fly. """
	propxml="""
			<popup name="button3">
			<menuitem name="Item 3" verb="About" label="_About" pixtype="stock" pixname="gtk-about"/>
			<separator/>
			<menuitem name="Item 3" verb="Switch" label="Sw_itch Modes (12/24 Hour)" pixtype="stock" pixname="gtk-help"/>
			</popup>
			"""
	verbs = [("About", show_about), ("Switch", switch_modes)]
	applet.setup_menu(propxml, verbs, None)

def show_about(args, kwords):
	""" Shows the About dialog. """
	abox = gtk.AboutDialog()
	aboutimg = gtk.gdk.pixbuf_new_from_file("prince_sml.jpg")

	abox.set_name("Fuzzy Clock Applet")
	abox.set_version("1.0")
	abox.set_copyright("Copyright 2008 tdr <dsm.iv.tr@gmail.com> and contributors.")
	abox.set_comments("A fuzzy clock applet for the GNOME 2 panel.\nMore-or-less accurate to the minute. :)")
	abox.set_logo(aboutimg)

	response = abox.run()
	abox.hide()

def switch_modes(args, kwords):
	global mode
	global buttn_refer

	if mode == 24:
		mode = 12
	else:
		mode = 24

	update_fuzzlabel(buttn_refer)

def update_clock():
	global mode

	""" This function returns a string with the correct fuzzy time. """
	# First, get current time.
	#
	time_obj = datetime.today()
	time_mins = time_obj.minute
	time_hrs = time_obj.hour

	# Earlier parts of the hour get 'past' addendums.
	# Later parts of the hour get 'to' addendums.
	#
	if time_mins >= 0 and time_mins < 10:
		cur_fuzz = ''

	elif time_mins >= 10 and time_mins < 15:
		# ten past
		cur_fuzz = timeways[0] + position[0]

	elif time_mins >= 15 and time_mins < 20:
		# quarter past
		cur_fuzz = timeways[1] + position[0]

	elif time_mins >= 20 and time_mins < 25:
		# twenty past
		cur_fuzz = timeways[2] + position[0]

	elif time_mins >= 25 and time_mins < 30:
		# twenty-five past
		cur_fuzz = timeways[3] + position[0]

	elif time_mins >= 30 and time_mins < 35:
		# half past
		cur_fuzz = timeways[4] + position[0]

	elif time_mins >= 35 and time_mins < 40:
		# twenty-five to
		cur_fuzz = timeways[3] + position[1]

	elif time_mins >= 40 and time_mins < 45:
		# twenty to
		cur_fuzz = timeways[2] + position[1]

	elif time_mins >= 45 and time_mins < 50:
		# quarter to
		cur_fuzz = timeways[1] + position[1]

	elif time_mins >= 50 and time_mins < 60:
		# ten to
		cur_fuzz = timeways[0] + position[1]
	else:
		print "We shouldn't be here. Something is wrong with the system clock, python, or this applet."

	# 24-hour versus 12-hour clock.
	#
	if mode == 12 and time_hrs != 12 and time_hrs != 0:
		time_hrs -= 12

	# Apply addendums.
	#
	if time_mins > 35:
		cur_fuzz += numerics[(time_hrs + 1)]
	else:
		cur_fuzz += numerics[time_hrs]

	return cur_fuzz

# Parse commandline args.
#
if len(sys.argv) == 2:
	if sys.argv[1] == "--in-window":
		mainwin = gtk.Window(gtk.WINDOW_TOPLEVEL)

		mainwin.set_title("Fuzzy Clock")
		mainwin.connect("destroy", gtk.main_quit)
		applet = gnomeapplet.Applet()

		# Do the main init work here.
		#
		fuzzclock_factory(applet, None)

		applet.reparent(mainwin)
		mainwin.show_all()

		gtk.main()
		sys.exit()

gnomeapplet.bonobo_factory("OAFIID:GNOME_FuzzClock_Factory", gnomeapplet.Applet.__gtype__, "FuzzClock", "0", fuzzclock_factory)
