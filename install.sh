#!/bin/sh

# INSTALL LOCATIONS: EDIT ME IF DESIRED
#
PREFX=/usr

echo "This will install the Fuzzy Clock GNOME panel applet."
echo ""
echo "If you choose to install somewhere other than the standard /usr"
echo "please be sure your Bonobo configuration is set up to look there."
echo "You may check /etc/bonobo-activation/bonobo-activation-config.xml"
echo "to confirm this."
echo ""
echo -n "I'll install to the prefix $PREFX. Is this OK? [y/n/q] "
read ANS

case "$ANS" in
	Y|y)
		# Install the files.
		#
		install -v -m 0755 -D FuzzClock.py $PREFX/bin/FuzzClock.py
		install -v -m 0644 -D FuzzClock.server $PREFX/lib/bonobo/servers/FuzzClock.server
		install -v -m 0644 -D prince_sml.jpg $PREFX/share/fuzzyclock-applet/prince_sml.jpg

		# Replace the location of the logo image and .py script
		# in the installed python code.
		#
		sed	-i.original \
			-e "s:prince_sml.jpg:$PREFX/share/fuzzyclock-applet/prince_sml.jpg:" \
			-e "s:\/usr\/bin\/FuzzClock.py:$PREFX/bin/FuzzClock.py:" \
			$PREFX/bin/FuzzClock.py

		break
	;;

	*)
		echo "Aborted."

		break
	;;
esac
